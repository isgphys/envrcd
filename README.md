envrcd
======

An environment switcher for shared system accounts.

Motivation
----------

An admin is probably more efficient and certainly much happier in his customized environment. That includes settings like aliases, colors, environment variables, program configurations etc. Of course this does not apply for real hacker gurus in front of a green shimmering CRT screen, they just need one color, that is green, and the defaults. But some noob admins, like me, don't even know how to move the cursor if vim is in compatibility mode. With only `/root/.bashrc` all admins need to agree on a common environment. Nobody will be completely happy. This project tries to solve that problem.

Reserved files in `envrcd.d`
----------------------------

These files should only exist locally and are ignored by git

- `00-local`: `ENVRCD_SET_*` (contains user settings, exists only in your private home)
- `20-host`: `ENVRCD_INVENTORY` (host specific environment vars, ansible managed)
- `90-host-late`: host aliases (host specific alias overrides, ansible managed)
- `95-host-late-local`: local only (host specific overrides, locally managed)

### 00-local: `~/.envrcd/envrcd.d/00-local`

Create this file to set local user settings. Only use this in your users home directory. Do not use this file for root.

```bash
export ENVRCD_SET_USER="<username>"    # set envrcd user name
export ENVRCD_SET_PROFILE="<profile>"  # set envrcd profile to use (leave empty for default)
export ENVRCD_SET_DEBUG="true"         # enable debugging messages
export ENVRCD_NO_ENVRCD="true"         # abort loading advance envrcd settings, can be used should something go wrong
```

### 20-host: `~/.envrcd/envrcd.d/20-host`

Create this file to set host specific settings.

```bash
export ENVRCD_INVENTORY="testing"      # set inventory group
```

### 90-host-late: `~/.envrcd/envrcd.d/90-host-late`

Create this file to set host specific settings that should override possible user settings. Useful to define host specific aliases.

Basic operation
---------------

The script works by sending environment variables prefixed with `ENVRCD_SET_*` via SSH to the remote host. The following variables are supported:

- `ENVRCD_SET_USER`: Set this to your name
- `ENVRCD_SET_PROFILE`: Profile to load
- `ENVRCD_SET_DEBUG`: Set to enable debugging mode
- `ENVRCD_SET_NOENVRCD`: Set to disable advanced user envrcd and load system root defaults

Load order:

- The script first loads its configuration from `~/.envrcd/envrcd.d/`
- Then if `ENVRCD_SET_PROFILE` is set, it will try to load the profile from `~/.envrcd/profile/${ENVRCD_SET_PROFILE}`
- If it is unset it will load the defaults from `~/.encrcd/profile/default`

All following files are sourced inside the profile directory in this order:

- `envrcd`
- `envrcd.d/*`
- `.bashrc`

To load host specific settings place `20-host` and/or `90-host-late` in `~/.envrcd/envrcd.d/`. For example, we can set `ENVRCD_INVENTORY` to `production` or `testing` in `20-host`, which will show a different colored prompt. The default is to assume we are on a `production` system if this variable is not set.

Configuration for the user (on your workstation)
------------------------------------------------

Send the `ENVRCD_SET_*` environment vars in either `/etc/ssh/ssh_config` or `~/.ssh/config`:

```
SendEnv [...] ENVRCD_SET_*
```

### Minimalist variant

Export the following variables in your `~/.bashrc` for your normal account:

```bash
export ENVRCD_SET_USER="<username>"
```

### Or migrate your `~/.bashrc` to `envrcd`

Setup the files:

```bash
cd ~
git clone git@gitlab.phys.ethz.ch:core/envrcd.git .envrcd
mkdir .envrcd/profile/<my_profile>
mv .bashrc .envrcd/profile/<my_profile>/
ln -s .envrcd/.envrcd .bashrc
```

Create your local `~/.envrcd/envrcd.d/00-local` and set `ENVRCD_SET_USER` and `ENVRCD_SET_PROFILE`:

```bash
export ENVRCD_SET_USER="<username>"
export ENVRCD_SET_PROFILE="<my_profile>"
```

Commit and push your changes:

```bash
cd .envrcd
git add -A
git commit -m 'Add user <username>'
git push
```

Installation for root (server)
------------------------------

Setup the files:

```bash
ssh root@server
git clone git@gitlab.phys.ethz.ch:core/envrcd.git .envrcd
mv .bashrc .bashrc.old
ln -s .envrcd/.envrcd .bashrc
```

Accept the `ENVRCD_SET_*` environment vars in `/etc/ssh/sshd_config`:

```
AcceptEnv [...] ENVRCD_SET_*
```

Restart `sshd`:

```
systemctl restart sshd
```
