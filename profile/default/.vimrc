" --- general settings ---{{{
" load debian settings from $VIMRUNTIME/debian.vim
runtime! debian.vim

" insert custom path to &runtimepath
let &runtimepath=$ENVRCD_VIM_PATH.','.&runtimepath

syntax on               "enable syntax highlighting
set nocompatible        "use vim defaults instead of 100% vi compatibility
set encoding=utf8       "default file encoding
set ttyfast             "fast terminal
set mouse=              "disable mouse mode
filetype plugin on      "enable filetype plugins
filetype indent on      "automatic indentation according to filetype
"set autoindent          "preserve indentation on new line
set backspace=eol,start,indent  "allow backspace at end of line etc
set showmatch           "show matching parenthesis
"set linebreak           "break line at end of a word
"set textwidth=0         "don't break text after a certain length
set tabstop=4           "show existing tab with 4 spaces width
set shiftwidth=4        "when indenting with '>', use 4 spaces width
set softtabstop=4       "use 4 spaces per tab when editing
set expandtab           "when pressing tab, insert 4 spaces
set smarttab            "use sw at beginning of line, ts otherwise
set incsearch           "incremental search
set wrapscan            "continue search from top when reaching the end
set ignorecase          "search is not case-sensitive
set smartcase           "search case-sensitive for uppercase patterns
set hlsearch            "search pattern highlighting
set magic               "enable extended regexes
set spellsuggest=7      "show 7 suggestions when spellchecking
set nopaste             "fix middlemouse button paste (seems default setting)
set history=999         "increase history (default = 20)
set undolevels=999      "more undo (default=100)
set nobackup            "toggle keeping ~ backups of files when saving
set swapfile            "use swp file while a file is open
set modeline            "scan first/last lines of file for vim commands
set modelines=5         "number of lines checked for modelines
set display=lastline    "show beginning of broken line at bottom of window instead of displaying @
"set clipboard=unnamed   "make all operations such as yy, dd, p work with the clipboard
set wildmenu            "show matches for command-line completion
set wildmode=longest:full,full  "define how tab completion proceeds on partial match
set wildignore+=*.o,*.pdf       "Let tab completion ignore certain file types
set fillchars=fold:-
set t_Co=256            "use 256 colors

" temporary workaround for jessie's old vim version
if v:version >704 || v:version==704 && has('patch710')
  set listchars=eol:¬,tab:>-,trail:~,space:·  "define replacement for invisible characters (set invlist)
endif

" Disable auto-comment after breaking lines with formatoptions
au FileType * setl fo-=c fo-=r fo-=o
"}}}

" --- functions ---{{{
" --- map key to toggle opt ---
function MapToggle(key, opt)
  let cmd = ':set '.a:opt.'! \| set '.a:opt."?\<CR>"
  exec 'nnoremap '.a:key.' '.cmd
  exec 'inoremap '.a:key." \<C-O>".cmd
endfunction
command -nargs=+ MapToggle call MapToggle(<f-args>)

" --- toggle mouse=i on/off ---
function ToggleMouse()
  if (&mouse == 'i') || (&mouse == 'a')
    set mouse=
  else
    set mouse=i
  endif
endfunc

" --- Clean trailing spaces ---
function! CleanSpaces()
    %s/\s\+$//e
endfunction
"}}}

" --- keyboard mappings ---{{{
" --- F key toggles ---
" toggle paste mode (normal/insert)
MapToggle <F2> paste
set pastetoggle=<F2>
" toggle mouse mode (normal/insert)
nnoremap <F3> :call ToggleMouse()<CR>
inoremap <F3> <C-O>:call ToggleMouse()<CR>
" toggle list mode (normal/insert)
MapToggle <F4> list

" --- normal mappings ---
" toggle paste mode
MapToggle <Leader>p paste
" set mouse modes
nnoremap <Leader>m :set mouse=<CR>
nnoremap <Leader>mi :set mouse=i<CR>
nnoremap <Leader>ma :set mouse=a<CR>
" toggle list of invisible characters
MapToggle <Leader>w list
" remap space to fold/unfold
map <space> za
" remap \s to toggle spellchecking
map <Leader>s :set invspell<CR>
" remap \n to toggle display of line numbers
map <Leader>n :set invnu<CR>
" remap \h to stop hlsearch until next search
map <Leader>h :noh<CR>
"}}}

" --- colorscheme ---{{{
" use pablo color scheme
colorscheme pablo
" load jellybeans if available
:silent! colorscheme jellybeans
" make comments more readable with lighter grey
highlight Comment ctermfg=Grey

" --- background ---
" use no background in terminal instead of grey
highlight Normal ctermbg=none
highlight NonText ctermbg=none
highlight SpecialKey ctermbg=none

" --- highlights ---
" tweak todo to be brighter
highlight Todo guifg=Cyan guibg=Black gui=none ctermbg=36 cterm=none ctermfg=9
" highlight superfluous whitespace in orange
highlight ExtraWhiteSpace guifg=White guibg=#DF5F00 gui=bold ctermfg=255 ctermbg=166 cterm=bold
"}}}

" --- whitespace ---{{{
" match whitespace
match ExtraWhitespace /\s\+$/
" enable when opening buffer
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
" disable when entering insert mode
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
" enable when leaving insert mode
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()
"}}}

" --- vim modeline ---{{{
" vim: set foldmethod=marker:
"}}}

" --- httplog syntax (nginx, apache) ---{{{
autocmd BufRead,BufNewFile /var/log/nginx/** set filetype=httplog
"}}}
