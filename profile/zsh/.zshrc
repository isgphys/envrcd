# set screen window name
if [[ $TERM == "screen" ]]; then
    preexec() { echo -ne "\033k$1\033\\" }
fi

# shell prompt
export PS1='%n@%m:%~%(!.#.$) '
unset RPS1

# history
HISTSIZE=4000
SAVEHIST=4000
HISTFILE=~/.history

# debian packages
if [[ "${ENVRCD_USER}" == "tarzeau" ]]; then
    # set favourite editor, emacs is an alternative
    EDITOR=mcedit; export EDITOR

    # set names
    DEBEMAIL="alex@aiei.ch"; export DEBEMAIL
    DEBFULLNAME="Alex Myczko"; export DEBFULLNAME
    GIT_AUTHOR_EMAIL="gurkan@phys.ethz.ch"; export GIT_AUTHOR_EMAIL
    GIT_AUTHOR_NAME="Gürkan Myczko"; export GIT_AUTHOR_NAME
    GIT_COMMITER_EMAIL="gurkan@phys.ethz.ch"; export GIT_COMMITER_EMAIL
    GIT_COMMITER_NAME="Gürkan Myczko"; export GIT_COMMITER_NAME
fi
