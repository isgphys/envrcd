# /root/.bashrc for D-PHYS servers, distributed via dphys-config

if [ "$PS1" ]; then
	export PS1='\[\e[1m\][ \[\e[31m\]\u@\h:\w/\[\e[m\]$(git branch 2>&1 | fgrep "*" | awk "{print \" (\"\\$2\")\"}") \[\e[1m\]>\n\[\e[1m\]\$\[\e[m\] '
fi

shopt -s checkwinsize
shopt -s histappend

ulimit -f unlimited

export LANG=en_US.UTF-8
export PAGER='less -s'
export LESS='-R'
export LESSCHARSET='utf-8'
export LS_OPTIONS='--color=auto'
eval "`dircolors`"
alias ls='ls $LS_OPTIONS'

alias mv='mv -i'
alias cp='cp -i'
alias ll='ls -hlF'
alias la='ls -AF'
alias lla='ls -hlAF'
alias nano='LC_ALL=en_US.UTF-8 nano'
alias ccze='ccze -A -o nolookups'

alias aptitude-just-recommended='aptitude -o "Aptitude::Pkg-Display-Limit=!?reverse-depends(~i) ~M !?essential"'
alias aptitude-also-via-dependency='aptitude -o "Aptitude::Pkg-Display-Limit=~i !~M ?reverse-depends(~i) !?essential"'
alias aptitude-review-unmarkauto-libraries='aptitude -o "Aptitude::Pkg-Display-Limit=( ^lib !-dev$ !-dbg$ !-utils$ !-tools$ !-bin$ !-doc$ !^libreoffice | -data$ | -common$ | -base$ !^r-base ) !~M"'

# colorized cat using pygments
function ccat {
    for iarg in $@
    do
        ext=${iarg##*.}
        # force md lexer on .markdown files
        if [ $ext == 'markdown' ]; then
            pygmentize -O "style=native" -f console256 -l md $iarg
        else
            pygmentize -O "style=native" -f console256 -g $iarg
        fi
    done
}

# Colorful man pages
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;33m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[04;32m'
