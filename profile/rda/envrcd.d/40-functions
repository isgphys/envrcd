#!/bin/bash

# pyclean: delete __pycache__ and *.pyc files
function pyclean {
    find . \( -name '__pycache__' -o -name '*.pyc' \) -not -path './.venv/*' -delete
}

# gvimscp: open remote files using bash tab completion
function gvimscp {
    if [[ -z $1 ]]; then
        echo "usage: gvimscp [[user@]host1:]file1 ... [[user@]host2:]file2"
        return
    fi
    declare -a targs=()
    for iarg in $@
    do
        # adapt syntax for vim
        targ="scp://$(echo $iarg | sed -e 's@:/@//@' | sed -e 's@:@/@')"
        targs=("${targs[@]}" $targ)
    done
    gvim ${targs[@]}
}

# manv, mangv: open manpage in vim/gvim
function manv {
    man $* | col -bx | vim -c ":set ft=man" -R -;
}
function mangv {
    man $* | col -bx | gvim -c ":set ft=man" -R -;
}

# extract: unpack various types of archives
function extract {
    if [ -f $1 ] ; then
        case $1 in
            *.zip)  unzip $1    ;;
            *.rar)  unrar e $1  ;;
            *.tar)  tar xf $1   ;;
            *.tar.bz2)  tar xjf $1  ;;
            *.tar.gz)   tar xzf $1  ;;
            *.tbz2) tar xjf $1  ;;
            *.tgz)  tar xzf $1  ;;
            *.bz2)  bunzip2 $1  ;;
            *.gz)   gunzip $1   ;;
            *.Z)    uncompress $1   ;;
            *)  echo "unrecognized archive type"
        esac
    else
        echo "'$1' is not a valid file"
    fi
}

# cd: hack cd to allow more dots
function cd {
  if   [[ "x$*" == "x..." ]]; then
    cd ../..
  elif [[ "x$*" == "x...." ]]; then
    cd ../../..
  elif [[ "x$*" == "x....." ]]; then
    cd ../../..
  elif [[ "x$*" == "x......" ]]; then
    cd ../../../..
  else
    builtin cd "$@"
  fi
}

# gitmt: preview what a merge would do
function gitmt {
    git merge-tree $(git merge-base HEAD ${1}) HEAD ${1} | diff-so-fancy | less
}

# gitfbrm: remove a file including history from a git repo
function gitfbrm {
    git filter-branch --force --index-filter "git rm --cached --ignore-unmatch ${1}" --prune-empty --tag-name-filter cat -- --all
    git for-each-ref --format='delete %(refname)' refs/original | git update-ref --stdin
    git reflog expire --expire=now --all
    git gc --aggressive --prune=now
}

# gitfbrmr: remove a directory including history from a git repo
function gitfbrmr {
    git filter-branch --force --tree-filter "rm -rf ${1}" HEAD
    git for-each-ref --format='delete %(refname)' refs/original | git update-ref --stdin
    git reflog expire --expire=now --all
    git gc --aggressive --prune=now
}

# gitoblit: obliterate a file or directory
function gitoblit {
    git obliterate "${1}"
    git for-each-ref --format='delete %(refname)' refs/original | git update-ref --stdin
    git reflog expire --expire=now --all
    git gc --aggressive --prune=now
}

# gitx: git pull, status, add, commit, push
function gitx {
    local NO="\e[0m"  # NORMAL FONT
    local RE="\e[31m" # RED
    local GR="\e[32m" # GREEN
    local YE="\e[33m" # YELLOW
    local BL="\e[34m" # BLUE

    #echo -e "${BL}gitx: pull and fast-forward merge${NO}"
    #git pull --ff-only

    #if [ "$?" -ne "0" ]; then
    #    echo -e "${RE}error: fast-forward not possible${NO}"
    #    echo -e "${YE}fetch and merge or pull manually${NO}"
    #    return
    #fi

    echo -e "${BL}gitx: pull politely with rebase${NO}"
    git pull --rebase --autostash

    if [ "$?" -ne "0" ]; then
        echo -e "${RE}error: rebase not possible${NO}"
        echo -e "${YE}fetch and merge manually${NO}"
        return
    fi

    echo -e "${BL}gitx: show status${NO}"
    git status

    if [ "$?" -ne "0" ]; then
        echo -e "${RE}error: git status failed${NO}"
        return
    fi

    echo -e "${BL}gitx: stage all and commit${NO}"
    git add -A

    if [ "$?" -ne "0" ]; then
        echo -e "${RE}error: git add failed${NO}"
        return
    fi

    echo -e "${BL}gitx: show status${NO}"
    git status

    git commit -m "${@}"

    if [ "$?" -ne "0" ]; then
        echo -e "${RE}error: git commit failed${NO}"
        return
    fi

    echo -e "${BL}gitx: push changes${NO}"
    git push

    if [ "$?" -ne "0" ]; then
        echo -e "${RE}error: git push failed${NO}"
        return
    fi

    #echo -e "${BL}gitx: success!${NO}"
}
