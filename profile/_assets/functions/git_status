#!/bin/bash

function envrcd_function_git_status {
    local GITSTATUS=$(git status 2>&1)

    # function arguments and their defaults
    local CLEAN="${1}"
    local DIRTY="${2}"
    local DELIM="${3}"
    local PRE_CLEAN="${4:-[}"
    local PRE_DIRTY="${5:-[}"
    local POST="${6:-]}"
    local MASTER="${7}"

    # Retrieve current branch
    if [[ ${GITSTATUS} =~ On\ branch\ ([^[:space:]]+) ]]; then
        GITBRANCH="${BASH_REMATCH[1]}"
    elif [[ ${GITSTATUS} =~ HEAD\ detached\ (at|from)\ ([^[:space:]]+) ]]; then
        GITBRANCH="${BASH_REMATCH[2]}"
    fi

    if [[ ${GITSTATUS} =~ "ot a git repository" ]]; then
        # No git repo
        GITSTAT=""
    elif [[ ${GITSTATUS} =~ "must be run in a work tree" ]]; then
        # Bare git repo
        GITSTAT=""
    elif [[ ${GITSTATUS} =~ working\ (tree|directory)\ clean ]]; then
        # Clean repo
        GITSTAT="${PRE_CLEAN}${GITBRANCH}${POST}"
        # Display only if not on master branch
        if [ "${GITBRANCH}" == "master" ]; then
            GITSTAT="${PRE_CLEAN}${MASTER}${CLEAN}${POST}"
        fi
    else
        # Dirty repo
        GITSTAT="${PRE_DIRTY}${GITBRANCH}${DELIM}${DIRTY}${POST}"
        # Display only if not on master branch
        if [ "${GITBRANCH}" == "master" ]; then
            GITSTAT="${PRE_DIRTY}${MASTER}${DIRTY}${POST}"
        fi
    fi
}
